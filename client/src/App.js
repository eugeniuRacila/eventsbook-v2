import React, { Component } from "react";
import { BrowserRouter as Switch, Route } from "react-router-dom";

// import "./App.css";
import "./styles/main.scss";
import Main from "./pages/Main/Main";
import NavBar from "./components/NavBar/NavBar";
import Footer from "./components/Footer/Footer";
import Register from "./pages/Register/RegisterContainer";
import RegistrationDetails from "./pages/RegistrationDetails/RegistrationDetailsContainer";
import LogIn from "./pages/LogIn/LogInContainer";
import ForgotPassword from "./pages/ForgotPassword/ForgotPasswordContainer";
import LookForEvents from "./pages/LookForEvents/LookForEventsContainer";
import Event from "./pages/Event/EventContainer";
import Dashboard from "./pages/Dashboard/DashboardContainer";
import CreateEvent from "./pages/CreateEvent/CreateEventContainer";
import Stripe from "./pages/Stripe/Stripe";
import Page404 from "./pages/error/Page404/Page404";

import './App.css';
import './styles/main.scss';

class App extends Component {
  render() {
    return (
      <Switch>
        <div className="App">
          <div className="App__container">
            <NavBar />
            <Route exact path="/" component={Main} />
            <Route exact path="/log-in" component={LogIn} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/forgot-password" component={ForgotPassword} />
            <Route exact path="/register-details" component={RegistrationDetails} />
            <Route exact path="/look-for-events" component={LookForEvents} />
            <Route exact path="/event-id" component={LookForEvents} />
            <Route exact path="/event-id/:id" component={Event} />
            <Route exact path="/dashboard" component={Dashboard} />
            <Route exact path="/create-event" component={CreateEvent} />
            <Route exact path="/payment" component={Stripe} />

            {/* <Route component={Page404} /> */}
            <Footer />
          </div>
        </div>
      </Switch>
    );
  }
}

export default App;
