import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";

import "./EventCard.css";

class EventCard extends Component {
  render() {
    const {
      id,
      title,
      type,
      time,
      currentSignedAmount,
      maxAmount,
      thumbnail,
      city,
      address
    } = this.props;

    return (
      <div className="event">
        <div className="event__top">
          <img src={`/uploads/${thumbnail}`} alt={title} className="event__thumbnail" />
          <div className="event__type-and-amount">
            <p className="event__amount">{`${currentSignedAmount} / ${maxAmount}`}</p>
            <Link to={`/look-for-events?type=${type}`} className="event__type">{type}</Link>
          </div>
        </div>
        <Link to={{ pathname: `/event-id/${id}`, state: { id } }} className="event__bottom">
          <p className="event__title">{title}</p>
          <div className="event__address-and-time">
            <p className="event__address"><span style={{ textTransform: "capitalize" }}>{city}</span>, <span style={{ textTransform: "capitalize" }}>{address}</span></p>
            <p className="event__time">{time}</p>
          </div>
        </Link>
      </div>
    );
  }
}

export default withRouter(EventCard);
