import React, { Component } from "react";
import { Link } from "react-router-dom";

import EventCard from "./EventCard";

class EventCardContainer extends Component {
  render() {
    const {
      id,
      title,
      type,
      time,
      city,
      address,
      currentSignedAmount,
      maxAmount,
      thumbnail
    } = this.props;

    return (
      <EventCard
        id={id}
        title={title}
        type={type}
        time={time}
        city={city}
        address={address}
        currentSignedAmount={currentSignedAmount}
        maxAmount={maxAmount}
        thumbnail={thumbnail}
      />
    );
  }
}

export default EventCardContainer;