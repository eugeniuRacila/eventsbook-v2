import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./Footer.css";

class Footer extends Component {
  render() {
    return (
      <footer className="footer">
        <div className="footer__container">
          <div className="footer__section">
            <div className="footer__logo">
              <Link to="/">
                <svg width="142" height="32" viewBox="0 0 142 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M43.0906 21.1918C45.5259 21.1918 47.1559 20.0092 47.5394 18.1875L45.0273 18.0213C44.7525 18.7692 44.043 19.1591 43.1417 19.1591C41.7802 19.1591 40.9173 18.2578 40.9173 16.794V16.7876H47.6033V16.0398C47.6033 12.7031 45.5835 11.054 42.9883 11.054C40.0991 11.054 38.2262 13.1058 38.2262 16.1357C38.2262 19.2486 40.0735 21.1918 43.0906 21.1918ZM40.9173 15.1001C40.9684 13.9815 41.8249 13.0866 43.033 13.0866C44.2092 13.0866 45.0273 13.9304 45.0401 15.1001H40.9173ZM58.4139 11.1818H55.5311L53.4984 18.2067H53.3961L51.3571 11.1818H48.4806L51.9132 21H54.9814L58.4139 11.1818ZM63.9734 21.1918C66.4087 21.1918 68.0387 20.0092 68.4222 18.1875L65.9102 18.0213C65.6353 18.7692 64.9258 19.1591 64.0245 19.1591C62.663 19.1591 61.8001 18.2578 61.8001 16.794V16.7876H68.4862V16.0398C68.4862 12.7031 66.4663 11.054 63.8711 11.054C60.9819 11.054 59.109 13.1058 59.109 16.1357C59.109 19.2486 60.9563 21.1918 63.9734 21.1918ZM61.8001 15.1001C61.8512 13.9815 62.7077 13.0866 63.9158 13.0866C65.092 13.0866 65.9102 13.9304 65.9229 15.1001H61.8001ZM73.0245 15.3239C73.0309 14.0582 73.7852 13.3168 74.8846 13.3168C75.9776 13.3168 76.636 14.0327 76.6296 15.2344V21H79.3526V14.7486C79.3526 12.4602 78.0103 11.054 75.9648 11.054C74.5075 11.054 73.4528 11.7699 73.0117 12.9141H72.8967V11.1818H70.3015V21H73.0245V15.3239ZM86.7211 11.1818H84.8738V8.82955H82.1507V11.1818H80.8084V13.2273H82.1507V18.3217C82.138 20.2393 83.4483 21.1918 85.4235 21.1151C86.1266 21.0831 86.6252 20.9425 86.9 20.8594L86.4718 18.8267C86.3375 18.8587 86.0499 18.9162 85.7942 18.9162C85.2509 18.9162 84.8546 18.7116 84.8738 17.9574V13.2273H86.7211V11.1818ZM96.5584 13.9815C96.3155 12.1726 94.8581 11.054 92.378 11.054C89.8659 11.054 88.2104 12.2173 88.2168 14.1094C88.2104 15.5795 89.1373 16.532 91.0549 16.9155L92.7551 17.2543C93.6117 17.4268 94.0016 17.7401 94.0144 18.2322C94.0016 18.8139 93.3688 19.2294 92.4164 19.2294C91.4448 19.2294 90.7992 18.8139 90.633 18.0149L87.9547 18.1555C88.2104 20.0348 89.8084 21.1918 92.41 21.1918C94.954 21.1918 96.7757 19.8942 96.7821 17.9574C96.7757 16.5384 95.8489 15.6882 93.9441 15.2983L92.1671 14.9403C91.253 14.7422 90.9142 14.429 90.9206 13.956C90.9142 13.3679 91.579 12.9844 92.4228 12.9844C93.3688 12.9844 93.9313 13.5021 94.0655 14.1349L96.5584 13.9815ZM98.4968 21H101.181V19.4276H101.303C101.68 20.2457 102.505 21.1598 104.09 21.1598C106.327 21.1598 108.072 19.3892 108.072 16.1037C108.072 12.7287 106.25 11.054 104.096 11.054C102.453 11.054 101.667 12.032 101.303 12.831H101.22V7.90909H98.4968V21ZM101.162 16.0909C101.162 14.3395 101.904 13.2209 103.227 13.2209C104.576 13.2209 105.292 14.3906 105.292 16.0909C105.292 17.804 104.563 18.9929 103.227 18.9929C101.917 18.9929 101.162 17.8423 101.162 16.0909ZM114.355 21.1918C117.334 21.1918 119.188 19.1527 119.188 16.1293C119.188 13.0866 117.334 11.054 114.355 11.054C111.37 11.054 109.523 13.0866 109.523 16.1293C109.523 19.1527 111.37 21.1918 114.355 21.1918ZM114.368 19.0824C112.994 19.0824 112.291 17.8232 112.291 16.1101C112.291 14.397 112.994 13.1314 114.368 13.1314C115.717 13.1314 116.42 14.397 116.42 16.1101C116.42 17.8232 115.717 19.0824 114.368 19.0824ZM125.465 21.1918C128.444 21.1918 130.297 19.1527 130.297 16.1293C130.297 13.0866 128.444 11.054 125.465 11.054C122.48 11.054 120.632 13.0866 120.632 16.1293C120.632 19.1527 122.48 21.1918 125.465 21.1918ZM125.478 19.0824C124.103 19.0824 123.4 17.8232 123.4 16.1101C123.4 14.397 124.103 13.1314 125.478 13.1314C126.826 13.1314 127.529 14.397 127.529 16.1101C127.529 17.8232 126.826 19.0824 125.478 19.0824ZM132.106 21H134.829V17.8807L135.564 17.0433L138.243 21H141.432L137.603 15.4325L141.247 11.1818H138.121L134.976 14.9084H134.829V7.90909H132.106V21Z" fill="#1C85E8" />
                  <rect x="20.9346" y="20.9345" width="12.1596" height="6.97856" rx="3.48928" transform="rotate(135 20.9346 20.9345)" fill="#1C85E8" />
                  <rect x="29.832" y="16.9719" width="16.2833" height="6.97856" rx="3.48928" transform="rotate(135 29.832 16.9719)" fill="#97D0DA" />
                  <path fillRule="evenodd" clipRule="evenodd" d="M15.8504 26.0186C14.4878 24.656 14.4878 22.4467 15.8505 21.0841L18.4673 18.4672C19.8299 19.8299 19.8299 22.0392 18.4673 23.4018L15.8504 26.0186Z" fill="#ED4F60" />
                  <circle cx="24.8974" cy="16.9719" r="3.48928" transform="rotate(135 24.8974 16.9719)" fill="#ED4F60" />
                  <rect x="11.0654" y="11.0654" width="12.1596" height="6.97856" rx="3.48928" transform="rotate(-45 11.0654 11.0654)" fill="#EA4F5F" />
                  <rect x="2.16846" y="15.028" width="16.2833" height="6.97856" rx="3.48928" transform="rotate(-45 2.16846 15.028)" fill="#97D0DA" />
                  <path fillRule="evenodd" clipRule="evenodd" d="M16.1496 5.98122C16.1496 5.98122 16.1496 5.98122 16.1496 5.98123C17.5122 7.34388 17.5122 9.55316 16.1496 10.9158L13.5327 13.5327C13.5327 13.5327 13.5327 13.5327 13.5327 13.5327C12.1701 12.17 12.1701 9.96072 13.5327 8.59807L16.1496 5.98122Z" fill="#1C85E8" />
                  <circle cx="7.10304" cy="15.028" r="3.48928" transform="rotate(-45 7.10304 15.028)" fill="#1C85E8" />
                </svg>
              </Link>
            </div>
            <p className="footer__copy-rights">
              © 2019. All rights reserved.
            </p>
          </div>
          <div className="footer__section">
            <p className="footer__section-title">Look for events</p>
            <menu className="footer__section-menu">
              <li className="footer__section-menu-item">
                <Link to="/trending-now" className="footer__section-menu-item-link">Trending now</Link>
              </li>
              <li className="footer__section-menu-item">
                <Link to="/located-near" className="footer__section-menu-item-link">Located near to you</Link>
              </li>
              <li className="footer__section-menu-item">
                <Link to="/search-based-on-location" className="footer__section-menu-item-link">Search based on location</Link>
              </li>
              <li className="footer__section-menu-item">
                <Link to="/upcoming-events" className="footer__section-menu-item-link">Upcoming events</Link>
              </li>
              <li className="footer__section-menu-item">
                <Link to="/promoted-events" className="footer__section-menu-item-link">Promoted events</Link>
              </li>
            </menu>
          </div>
          <div className="footer__section">
            <p className="footer__section-title">About us</p>
            <menu className="footer__section-menu">
              <li className="footer__section-menu-item">
                <Link to="/our-mission" className="footer__section-menu-item-link">Our mission</Link>
              </li>
              <li className="footer__section-menu-item">
                <Link to="/team" className="footer__section-menu-item-link">Team</Link>
              </li>
              <li className="footer__section-menu-item">
                <Link to="/history" className="footer__section-menu-item-link">History</Link>
              </li>
              <li className="footer__section-menu-item">
                <Link to="/contact-us" className="footer__section-menu-item-link">Contact us</Link>
              </li>
            </menu>
          </div>
          <div className="footer__section">
            <p className="footer__section-title">FAQ</p>
            <menu className="footer__section-menu">
              <li className="footer__section-menu-item">
                <Link to="/faq/how-to-sign-to-an-event" className="footer__section-menu-item-link">How to sign to an event</Link>
              </li>
              <li className="footer__section-menu-item">
                <Link to="/faq/prices" className="footer__section-menu-item-link">Prices</Link>
              </li>
              <li className="footer__section-menu-item">
                <Link to="/faq/ticket-refund" className="footer__section-menu-item-link">Ticket refund</Link>
              </li>
              <li className="footer__section-menu-item">
                <Link to="/faq/ticket-delivery" className="footer__section-menu-item-link">Ticket delivery</Link>
              </li>
              <li className="footer__section-menu-item">
                <Link to="/faq/event-streaming" className="footer__section-menu-item-link">Event streaming</Link>
              </li>
            </menu>
          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;
