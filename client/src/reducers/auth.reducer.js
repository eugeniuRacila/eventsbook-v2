import {
  FETCH_AUTH_BEGIN,
  FETCH_AUTH_SUCCESS,
  FETCH_AUTH_ERROR,
  LOGIN_AUTH_BEGIN,
  LOGIN_AUTH_SUCCESS,
  LOGIN_AUTH_ERROR
} from "../actions/auth.actions";

const initialState = {
  isAuth: false,
  email: null,
  firstName: null,
  lastName: null,
  loading: false,
  error: null
};

export default function authReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_AUTH_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      };

    case FETCH_AUTH_SUCCESS:
      return {
        email: action.payload.auth.email,
        firstName: action.payload.auth.firstName,
        lastName: action.payload.auth.lastName,
        error: null,
        isAuth: true,
        loading: false
      };

    case FETCH_AUTH_ERROR:
      console.log("Payload values :: ");
      console.dir(action.payload.error.response);

      return {
        error: {
          status: action.payload.error.response.status,
          statusTes: action.payload.error.response.statusText,
          data: action.payload.error.response.data.error
        },
        email: null,
        firstName: null,
        lastName: null,
        isAuth: false,
        loading: false
      };

    case LOGIN_AUTH_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      };

    case LOGIN_AUTH_SUCCESS:
      console.log('payload ::', action.payload);
      return {
        email: action.payload.email,
        firstName: action.payload.firstName,
        lastName: action.payload.lastName,
        error: null,
        isAuth: true,
        loading: false
      };

    case LOGIN_AUTH_ERROR:
      console.log("Payload values :: ");
      console.dir(action.payload.error.response);

      return {
        error: {
          status: action.payload.error.response.status,
          statusTes: action.payload.error.response.statusText,
          data: action.payload.error.response.data.error
        },
        email: null,
        firstName: null,
        lastName: null,
        isAuth: false,
        loading: false
      };

    default:
      return state;
  }
}
