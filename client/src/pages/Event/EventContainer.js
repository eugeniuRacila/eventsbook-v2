import React, { Component } from "react";
import axios from "axios";
import { withRouter } from "react-router-dom";
import Event from "./Event";

class EventContainer extends Component {
  state = {
    componentInfo: {}
  }

  componentDidMount = async () => {
    const {
      id
    } = this.props.location.state;

    const response = await axios.get(`http://localhost:5000/api/events/event/${id}`);
    console.log('response ::', response);

    this.setState({
      componentInfo: response.data.event
    })
  }

  render() {

    return (
      <Event
        componentInfo={this.state.componentInfo}
        buyTicket={() => this.props.history.push('/payment', {rubla: this.state.componentInfo.price, currency: this.state.componentInfo.currency})}
      />
    );
  }
}

export default withRouter(EventContainer);
