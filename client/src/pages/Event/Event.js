import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./Event.css";

class Event extends Component {
  render() {
    const {
      title,
      description,
      address,
      country,
      city,
      date,
      time,
      time_till,
      thumbnail,
      price,
      currency
    } = this.props.componentInfo;

    const rubla = currency && currency.toUpperCase();

    return (
      <div className="event-page">
        <div className="event-page__head">
          <div className="event-page__data">
            <div className="event-page__information">
              <h2 className="event-page__title">{title}</h2>
              <p className="event-page__address">{date}, {time} to {time_till} · Strada <span style={{ textTransform: "capitalize" }}>{address}</span> · <span style={{ textTransform: "capitalize" }}>{city}</span> · <span style={{ textTransform: "capitalize" }}>{country}</span></p>
            </div>
            <div className="event-page__ticket-purchase">
              <p className="event-page__ticket-price">{price} {rubla}</p>
              <button className="event-page__buy-ticket"  onClick={this.props.buyTicket}>Buy ticket</button>
            </div>
          </div>
        </div>
        <div className="event-page__thumbnail">
          <img src={`/uploads/${thumbnail}`} alt={title} className="event-page__thumbnail-image" />
        </div>
        <div className="event-page__description-content">
          <div className="event-page__description-left">
            <div className="event-page__description-left_text">
              <p className="event-page__description-title">{title}</p>
              {/* <p className="event-page__description-message">Beautiful 4-bed, 2.5 bath home in Fort Worth, TX! Open-concept eat-in kitchen with center island and tile backsplash. Living area features cozy fireplace and laminate wood flooring. Large master bedroom has vaulted ceilings and plenty of room for a seating area.</p>
              <p className="event-page__description-message">En-suite master bath includes dual sinks, separate shower and tub, and walk-in closet. Second floor features bonus gameroom! Inspection report available upon request.</p> */}
              {
                description && description.map(paragraph => (
                  <p className="event-page__description-message">{paragraph}</p>
                ))
              }
            </div>
            <div className="event-page__features">
              <div className="event-page__feature-box">
                <div className="event-page__feature-box_icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24" fill="none" stroke="#1C85E8" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-disc"><circle cx="12" cy="12" r="10" /><circle cx="12" cy="12" r="3" /></svg>
                </div>
                <p className="event-page__feature-box_text">Music and background songs</p>
              </div>
              <div className="event-page__feature-box">
                <div className="event-page__feature-box_icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24" fill="none" stroke="#1C85E8" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-disc"><circle cx="12" cy="12" r="10" /><circle cx="12" cy="12" r="3" /></svg>
                </div>
                <p className="event-page__feature-box_text">Music and background songs</p>
              </div>
              <div className="event-page__feature-box">
                <div className="event-page__feature-box_icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24" fill="none" stroke="#1C85E8" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-disc"><circle cx="12" cy="12" r="10" /><circle cx="12" cy="12" r="3" /></svg>
                </div>
                <p className="event-page__feature-box_text">Music and background songs</p>
              </div>
              <div className="event-page__feature-box">
                <div className="event-page__feature-box_icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24" fill="none" stroke="#1C85E8" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-disc"><circle cx="12" cy="12" r="10" /><circle cx="12" cy="12" r="3" /></svg>
                </div>
                <p className="event-page__feature-box_text">Music and background songs</p>
              </div>
              <div className="event-page__feature-box">
                <div className="event-page__feature-box_icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24" fill="none" stroke="#1C85E8" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-disc"><circle cx="12" cy="12" r="10" /><circle cx="12" cy="12" r="3" /></svg>
                </div>
                <p className="event-page__feature-box_text">Music and background songs</p>
              </div>
            </div>
          </div>
          <div className="event-page__description-right">
            <div className="event-page__guarantee-box">
              <div className="event-page__guarantee-box-inner">
                <div className="event-page__guarantee-box-inner_text">
                  <div className="event-page__guarantee-box-inner_icon">
                    <img className="event-page__guarantee-box-inner_icon-src" src="https://i.imgur.com/l1ErT7d.png" alt="shield" />
                  </div>
                  <div className="event-page__guarantee-box-inner_content">
                    <p className="event-page__guarantee-box-inner_title"><span>Eventsbook</span> Guarantee</p>
                    <p className="event-page__guarantee-box-inner_description">30-day satisfaction guarantee Certified 180-point inspection 2-year home warranty</p>
                  </div>
                </div>
                <button className="event-page__guarantee-box-inner_button">Learn about this</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Event;
