import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import axios from "axios";
import { connect } from "react-redux";

import LogIn from "./LogIn";
import { fetchLoginAuth } from "../../actions/auth.actions";
import { dispatch } from "rxjs/internal/observable/pairs";

export class LogInContainer extends Component {
  state = {
    email: "eugeniu.racila@gmail.com",
    password: "123456",
    error: null
  };

  onChangeHandler = event => {
    return this.setState({
      [event.target.name]: event.target.value
    });
  };

  onSubmitHandler = async event => {
    event.preventDefault();

    const {
      email,
      password
    } = this.state;

    await this.props.dispatch(fetchLoginAuth({ email, password }));

    // Redirect to dashboard page after login in
    this.props.history.push("/dashboard");
  }

  render() {
    const {
      email,
      password,
      error
    } = this.state;

    return (
      <div>
        <LogIn
          onChangeHandler={this.onChangeHandler}
          onSubmitHandler={this.onSubmitHandler}
          email={email}
          password={password}
          error={error}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    isAuth: state.auth.isAuth
  };
}

export default withRouter(connect(mapStateToProps)(LogInContainer));
