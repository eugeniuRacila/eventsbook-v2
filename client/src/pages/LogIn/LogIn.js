import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./LogIn.scss";

export class LogIn extends Component {
  render() {
    const {
      email,
      password,
      onChangeHandler,
      error,
      onSubmitHandler
    } = this.props;

    return (
      <div className="authentication-page">
        <div className="authentication-page__form-content">
          <div className="authentication-page__form-data">
            <p className="title-medium authentication-page__title-medium">
              Log in to your account
            </p>
            <form onSubmit={onSubmitHandler} className="sign-in-form">
              <div className="sign-in-form__inputs">
                <label className="sign-in-form__label">
                  <input
                    className="sign-in-form__input sign-in-form__input--email"
                    pattern="[^@\s]+@[^@\s]+\.[^@\s]+"
                    onChange={onChangeHandler}
                    type="email"
                    value={email}
                    name="email"
                    autoCorrect="off"
                    spellCheck="false"
                    required
                  />
                  <span
                    className={
                      email
                        ? "sign-in-form__label-text sign-in-form__label-text--value"
                        : "sign-in-form__label-text"
                    }
                  >
                    Email
                  </span>
                  <span className="sign-in-form__icon">
                    <svg
                      className="feather feather-at-sign sc-dnqmqq jxshSx"
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      stroke="currentColor"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      aria-hidden="true"
                      data-reactid="156"
                    >
                      <circle cx="12" cy="12" r="4" />
                      <path d="M16 8v5a3 3 0 0 0 6 0v-1a10 10 0 1 0-3.92 7.94" />
                    </svg>
                  </span>
                </label>
                <label className="sign-in-form__label">
                  <input
                    className="sign-in-form__input sign-in-form__input--password"
                    onChange={onChangeHandler}
                    type="password"
                    value={password}
                    name="password"
                    autoCorrect="off"
                    spellCheck="false"
                    required
                  />
                  <span
                    className={
                      password
                        ? "sign-in-form__label-text sign-in-form__label-text--value"
                        : "sign-in-form__label-text"
                    }
                  >
                    Password
                  </span>
                  <span className="sign-in-form__icon">
                    <svg
                      className="feather feather-lock sc-dnqmqq jxshSx"
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      stroke="currentColor"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      aria-hidden="true"
                      data-reactid="746"
                    >
                      <rect x="3" y="11" width="18" height="11" rx="2" ry="2" />
                      <path d="M7 11V7a5 5 0 0 1 10 0v4" />
                    </svg>
                  </span>
                </label>
                <div className="sign-in-form__helper">
                  <p className="sign-in-form__error">
                    {error ? (
                      <>
                        <span className="sign-in-form__error-icon">
                          <svg
                            class="feather feather-alert-circle sc-dnqmqq jxshSx"
                            xmlns="http://www.w3.org/2000/svg"
                            width="24"
                            height="24"
                            viewBox="0 0 24 24"
                            fill="none"
                            stroke="currentColor"
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            aria-hidden="true"
                            data-reactid="46"
                          >
                            <circle cx="12" cy="12" r="10" />
                            <line x1="12" y1="8" x2="12" y2="12" />
                            <line x1="12" y1="16" x2="12" y2="16" />
                          </svg>
                        </span>
                        Provide the password
                      </>
                    ) : null}
                  </p>
                  <Link
                    className="sign-in-form__forgot-password"
                    to="forgot-password"
                  >
                    Forgot password?
                  </Link>
                </div>
              </div>

              <button
                className="primary-large-button sign-in-form__register"
              >
                Log in
              </button>
              <button className="third-party-login-button sign-in-form__register">
                Log in via Google
              </button>
              <button className="third-party-login-button sign-in-form__register">
                Log in via Facebook
              </button>
            </form>
            <p className="authentication-page__question">
              Need an Eventsbook account?{" "}
              <Link to="/register" className="authentication-page__link">
                Create account
              </Link>
            </p>
          </div>
          <div className="authentication-page__footer-rights">
            <p className="authentication-page__copy-rights">
              © 2019 All Rights Reserved. Eventsbook® Ltd.
            </p>
            <p className="authentication-page__copy-rights">
              Cookie Preferences, Privacy, and Terms.
            </p>
          </div>
        </div>
        <div className="authentication-page__image" />
      </div>
    );
  }
}

export default LogIn;
