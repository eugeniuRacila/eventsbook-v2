import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./ForgotPassword.scss";

export class ForgotPassword extends Component {
  render() {
    const {
      emailValue,
      onChangeHandler,
      emailIsValid,
      onClick,
      isFetching,
      fetchSuccess,
      fetchError
    } = this.props;

    return (
      <div className="authentication-page">
        <div className="authentication-page__form-content">
          <div className="authentication-page__form-data">
            <p className="title-medium authentication-page__title-medium">
              Forgot password?
            </p>
            <form action="" className="sign-in-form">
              <div className="sign-in-form__inputs">
                <label className="sign-in-form__label">
                  <input
                    className="sign-in-form__input sign-in-form__input--email"
                    pattern="[^@\s]+@[^@\s]+\.[^@\s]+"
                    onChange={onChangeHandler}
                    type="email"
                    name="email"
                    autoCorrect="off"
                    spellCheck="false"
                    required
                  />
                  <span
                    className={
                      emailValue
                        ? "sign-in-form__label-text sign-in-form__label-text--value"
                        : "sign-in-form__label-text"
                    }
                  >
                    Email {isFetching && (<p>Is fetching</p>)}
                  </span>
                  <span className="sign-in-form__icon">
                    <svg
                      className="feather feather-at-sign sc-dnqmqq jxshSx"
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      stroke="currentColor"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      aria-hidden="true"
                      data-reactid="156"
                    >
                      <circle cx="12" cy="12" r="4" />
                      <path d="M16 8v5a3 3 0 0 0 6 0v-1a10 10 0 1 0-3.92 7.94" />
                    </svg>
                  </span>
                </label>
              </div>
              {fetchError && (<p>Fetch error</p>)}
              <button
                onClick={onClick}
                disabled={!emailIsValid}
                className="primary-large-button sign-in-form__register"
              >
                Reset password
              </button>
            </form>
            <p className="authentication-page__question">
              Back to{" "}
              <Link to="/log-in" className="authentication-page__link">
                Log in
              </Link>{" "}
              page
            </p>
          </div>
          <div className="authentication-page__footer-rights">
            <p className="authentication-page__copy-rights">
              © 2019 All Rights Reserved. Eventsbook® Ltd.
            </p>
            <p className="authentication-page__copy-rights">
              Cookie Preferences, Privacy, and Terms.
            </p>
          </div>
        </div>
        <div className="authentication-page__image" />
      </div>
    );
  }
}

export default ForgotPassword;
