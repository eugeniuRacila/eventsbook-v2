import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import axios from "axios";

import Register from "./ForgotPassword";

export class ForgotPasswordComponent extends Component {
  state = {
    email: "",
    emailIsValid: false,
    isFetching: false,
    fetchSuccess: null,
    fetchError: null
  };

  onChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value,
      emailIsValid: event.target.checkValidity()
    });
  };

  onClick = async event => {
    event.preventDefault();

    const {
      emailIsValid
    } = this.state;

    try {
      if (emailIsValid) {
        this.setState({
          isFetching: true
        });

        console.log('fetching..');

        const response = await axios({
          method: "POST",
          url: "http://localhost:5000/api/auth/forgot-password",
          headers: {
            "Content-Type": "application/json"
          },
          data: JSON.stringify({
            email: this.state.email
          })
        });

        console.log('Respone ----> ::', response);

        switch (response.status) {
          case 200:
            return this.setState({
              isFetching: false,
              fetchSuccess: true,
              fetchError: null
            });

          case 404:
            return this.setState({
              isFetching: false,
              fetchSuccess: null,
              fetchError: true
            });
        }
      }
    } catch (error) {
      // switch (error.response.status) {
      //   case 400:
      //     return this.setState({
      //       isFetching: false,
      //       fetchSuccess: null,
      //       fetchError: true
      //     });
      // }
      console.log("TYPE OF ERROR", typeof error);
      console.log('Error while fetching the forgot password ::', error.response);
    }
  }

  render() {
    const {
      isFetching,
      fetchSuccess,
      fetchError
    } = this.state;

    return (
      <div>
        <Register
          onChangeHandler={this.onChangeHandler}
          emailValue={this.state.email}
          emailIsValid={this.state.emailIsValid}
          onClick={this.onClick}
          isFetching={isFetching}
          fetchSuccess={fetchSuccess}
          fetchError={fetchError}
        />
      </div>
    );
  }
}

export default withRouter(ForgotPasswordComponent);
