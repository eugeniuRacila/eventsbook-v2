import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from 'axios';

import LookForEvents from "./LookForEvents";

class LookForEventsContainer extends Component {
  state = {
    // eventsList: []
    eventList: [
      {
        title: 'Some kind of a random title for an event so the visitors can see it',
        type: 'dancing',
        time: '17:00',
        locationAddress: 'Chișinău, Liviu Delean 3/1',
        currentSignedAmount: 27,
        maxAmount: 70,
        thumbnailUrl: 'https://images.unsplash.com/photo-1504609813442-a8924e83f76e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80'
      },
      {
        title: 'Some kind of a random title for an event so the visitors can see it',
        type: 'dancing',
        time: '17:00',
        locationAddress: 'Chișinău, Liviu Delean 3/1',
        currentSignedAmount: 27,
        maxAmount: 70,
        thumbnailUrl: 'https://images.unsplash.com/photo-1504609813442-a8924e83f76e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80'
      },
      {
        title: 'Some kind of a random title for an event so the visitors can see it',
        type: 'dancing',
        time: '17:00',
        locationAddress: 'Chișinău, Liviu Delean 3/1',
        currentSignedAmount: 27,
        maxAmount: 70,
        thumbnailUrl: 'https://images.unsplash.com/photo-1504609813442-a8924e83f76e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80'
      },
      {
        title: 'Some kind of a random title for an event so the visitors can see it',
        type: 'dancing',
        time: '17:00',
        locationAddress: 'Chișinău, Liviu Delean 3/1',
        currentSignedAmount: 27,
        maxAmount: 70,
        thumbnailUrl: 'https://images.unsplash.com/photo-1504609813442-a8924e83f76e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80'
      }
    ]
  }

  componentDidMount = async () => {
    const response = await axios.get('http://localhost:5000/api/events');
    console.log('response', response);

    this.setState({
      eventList: response.data.events
    });
  }

  render() {
    return (
      <LookForEvents eventList={this.state.eventList} />
    );
  }
}

export default LookForEventsContainer;
