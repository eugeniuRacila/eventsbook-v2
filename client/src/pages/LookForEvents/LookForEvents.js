import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./LookForEvents.css";
import EventCard from "../../components/EventCard/EventCardContainer";

class LookForEvents extends Component {
  render() {
    const { eventList } = this.props;
    return (
      <div className="look-for-events-page">
        {
          eventList && eventList.map(event => {
            console.log('EVENT ::', event);

            return (
              < EventCard
                id={event._id}
                title={event.title}
                type={event.category}
                time={event.time}
                currentSignedAmount={event.current_amount_people}
                maxAmount={event.max_amount_people}
                thumbnail={event.thumbnail}
                city={event.city}
                address={event.address}
              />
            )
          })
        }
      </div>
    );
  }
}

export default LookForEvents;
