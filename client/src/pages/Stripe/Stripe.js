import React, { Component } from "react";
import axios from "axios";
import {CardElement, injectStripe, Elements, StripeProvider} from 'react-stripe-elements';
import CheckoutForm from './CheckoutForm';
import './Stripe.css';

class Stripe extends Component {
  state = {
    success: false
  };


  render() {
    const {success} = this.state;
    console.info(success);
    return (
      <div className="event-page-stripe">
          <StripeProvider apiKey="pk_test_TYooMQauvdEDq54NiTphI7jx">
            {
              ( !  success &&(
                <div className="stripe">
                  <h1 className='blue-block__title title'>Va rugăm să întroduceți datele cardului bancar</h1>

                  <h2 className='blue-block__title titleSub'>Costul este de {this.props.location.state.rubla} {this.props.location.state.currency.toUpperCase()}</h2>
                  <div className="container">
                    <Elements>
                      <CheckoutForm />
                    </Elements>
                    <button className='event-page__buy-ticket buttonSuccess' onClick={() => this.setState({success: true})}>Achita</button>
                  </div>
                </div>
              )) || (
                <h1 className='blue-block__title titleSuuces'>Tranzacția dvs a fost efectuată cu succes !</h1>
              )
            }
          </StripeProvider>
      </div>
    );
  }
}

export default Stripe;
