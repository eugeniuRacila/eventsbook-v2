import React, { Component } from 'react';
import { connect } from 'react-redux';

import './Dashboard.css';

class Dashboard extends Component {
  render() {
    const {
      firstName
    } = this.props;

    return (
      <div className="dashboard-page">
        <div className="dashboard-page__head">
          <p className="dashboard-page__welcome-message">👋 Welcome back, {firstName}</p>
          <button className="dashboard-page__button">Edit profile</button>
        </div>

        <div className="dashboard-page__box" style={{ display: "flex" }}>
          <div className="dashboard-page__box_icon"></div>
          <div className="dashboard-page__box_content">
            <h2 className="dashboard-page__box_title">See new events</h2>
            <p className="dashboard-page__box_description">You requested an offer on June 3rd 2019, our home expert are working on it. You should hear from us on June 5th 2019.</p>
          </div>
          <div className="dashboard-page__box_action" style={{ display: "flex", marginLeft: "auto" }}>
            <button className="dashboard-page__button dashboard-page__button--invert" style={{ height: "fit-content", display: "flex" }}>See all</button>
          </div>
        </div>
        <p className="dashboard-page__label">Events information</p>
        <div className="dashboard-page__box" style={{ display: "flex" }}>
          <div className="dashboard-page__box_icon"></div>
          <div className="dashboard-page__box_content">
            <h2 className="dashboard-page__box_title">Events location that you signed for. They are displayed on the map in the consecutive order by time.</h2>
            <p className="dashboard-page__box_description">Vivamus gravida arcu eu condimentum pretium. Proin augue velit, auctor vitae lectus non, consequat commodo justo. Duis hendrerit velit non urna lacinia interdum. Phasellus ornare efficitur magna quis feugiat. Phasellus eu orci eget elit venenatis vehicula. Proin fermentum erat sed eros hendrerit, vel eleifend lectus efficitur. Nullam vitae arcu eros.</p>
          </div>
        </div>
        <div className="dashboard-page__box" style={{ display: "flex" }}>
          <div className="dashboard-page__box_icon"></div>
          <div className="dashboard-page__box_content">
            <h2 className="dashboard-page__box_title">Your rate of wasting money on something as events. Here's a graph for the previous week.</h2>
            <p className="dashboard-page__box_description">Vivamus gravida arcu eu condimentum pretium. Proin augue velit, auctor vitae lectus non, consequat commodo justo. Duis hendrerit velit non urna lacinia interdum. Phasellus ornare efficitur magna quis feugiat.</p>
          </div>
        </div>
        <p className="dashboard-page__label">Your signed in events</p>
        <div className="dashboard-page__box" style={{ display: "flex", margin: "32px 0" }}>
          <div className="dashboard-page__box_thumbnail"></div>
          <div className="dashboard-page__box_content">
            <h2 className="dashboard-page__box_title">Your rate of wasting money on something as events...</h2>
            <p className="dashboard-page__box_description">Vivamus gravida arcu eu condimentum pretium. Proin augue velit, auctor vitae lectus non, consequat commodo justo. Duis hendrerit velit non urna lacinia interdum. Phasellus ornare efficitur magna quis feugiat.</p>
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    firstName: state.auth.firstName
  };
}

export default connect(mapStateToProps)(Dashboard);