import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./RegistrationDetails.scss";

export class RegistrationDetails extends Component {
  render() {
    const {
      password,
      confirmPassword,
      firstName,
      lastName,
      birthDate,
      currentStatus,
      country,
      city,
      genre,
      zipCode,
      phoneNumber,
      handleChange,
      handleSubmit
    } = this.props;

    return (
      <div className="authentication-page" style={{ height: "fit-content" }}>
        <div className="authentication-page__form-content--registration-details">
          <div className="authentication-page__form-data">
            <p className="title-medium authentication-page__title-medium">
              Tell us a bit about yourself
            </p>
            <form onSubmit={handleSubmit} className="sign-in-form" style={{ marginBottom: "0" }}>
              <div className="sign-in-form__inputs-row" style={{ display: "flex" }}>
                <div className="sign-in-form__inputs" style={{ flex: "1", marginRight: "5%", marginBottom: "15px" }}>
                  <label className="sign-in-form__label">
                    <input
                      className="sign-in-form__input sign-in-form__input--email"
                      onChange={handleChange}
                      type="text"
                      value={firstName}
                      name="firstName"
                      autoCorrect="off"
                      spellCheck="false"
                      required
                    />
                    <span
                      className={
                        firstName
                          ? "sign-in-form__label-text sign-in-form__label-text--value"
                          : "sign-in-form__label-text"
                      }
                    >
                      First Name
                  </span>
                    <span className="sign-in-form__icon">
                      <svg
                        className="feather feather-at-sign sc-dnqmqq jxshSx"
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        aria-hidden="true"
                        data-reactid="156"
                      >
                        <circle cx="12" cy="12" r="4" />
                        <path d="M16 8v5a3 3 0 0 0 6 0v-1a10 10 0 1 0-3.92 7.94" />
                      </svg>
                    </span>
                  </label>
                </div>
                <div className="sign-in-form__inputs" style={{ flex: "1", marginRight: "5%", marginBottom: "15px" }}>
                  <label className="sign-in-form__label">
                    <input
                      className="sign-in-form__input sign-in-form__input--email"
                      onChange={handleChange}
                      type="text"
                      value={lastName}
                      name="lastName"
                      autoCorrect="off"
                      spellCheck="false"
                      required
                    />
                    <span
                      className={
                        lastName
                          ? "sign-in-form__label-text sign-in-form__label-text--value"
                          : "sign-in-form__label-text"
                      }
                    >
                      Last name
                  </span>
                    <span className="sign-in-form__icon">
                      <svg
                        className="feather feather-at-sign sc-dnqmqq jxshSx"
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        aria-hidden="true"
                        data-reactid="156"
                      >
                        <circle cx="12" cy="12" r="4" />
                        <path d="M16 8v5a3 3 0 0 0 6 0v-1a10 10 0 1 0-3.92 7.94" />
                      </svg>
                    </span>
                  </label>
                </div>
                <div className="sign-in-form__inputs" style={{ flex: "1", marginBottom: "15px" }}>
                  <label className="sign-in-form__label">
                    <input
                      className="sign-in-form__input sign-in-form__input--email"
                      onChange={handleChange}
                      type="text"
                      value={genre}
                      name="genre"
                      autoCorrect="off"
                      spellCheck="false"
                      required
                    />
                    <span
                      className={
                        genre
                          ? "sign-in-form__label-text sign-in-form__label-text--value"
                          : "sign-in-form__label-text"
                      }
                    >
                      Genre
                  </span>
                    <span className="sign-in-form__icon">
                      <svg
                        className="feather feather-at-sign sc-dnqmqq jxshSx"
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        aria-hidden="true"
                        data-reactid="156"
                      >
                        <circle cx="12" cy="12" r="4" />
                        <path d="M16 8v5a3 3 0 0 0 6 0v-1a10 10 0 1 0-3.92 7.94" />
                      </svg>
                    </span>
                  </label>
                </div>
              </div>
              <div className="sign-in-form__inputs-row" style={{ display: "flex" }}>
                <div className="sign-in-form__inputs" style={{ flex: "1", marginRight: "5%", marginBottom: "15px" }}>
                  <label className="sign-in-form__label">
                    <input
                      className="sign-in-form__input sign-in-form__input--email"
                      onChange={handleChange}
                      type="text"
                      value={birthDate}
                      name="birthDate"
                      autoCorrect="off"
                      spellCheck="false"
                      required
                    />
                    <span
                      className={
                        birthDate
                          ? "sign-in-form__label-text sign-in-form__label-text--value"
                          : "sign-in-form__label-text"
                      }
                    >
                      Birth date
                  </span>
                    <span className="sign-in-form__icon">
                      <svg
                        className="feather feather-at-sign sc-dnqmqq jxshSx"
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        aria-hidden="true"
                        data-reactid="156"
                      >
                        <circle cx="12" cy="12" r="4" />
                        <path d="M16 8v5a3 3 0 0 0 6 0v-1a10 10 0 1 0-3.92 7.94" />
                      </svg>
                    </span>
                  </label>
                </div>
                <div className="sign-in-form__inputs" style={{ flex: "1", marginRight: "5%", marginBottom: "15px" }}>
                  <label className="sign-in-form__label">
                    <input
                      className="sign-in-form__input sign-in-form__input--email"
                      onChange={handleChange}
                      type="text"
                      value={phoneNumber}
                      name="phoneNumber"
                      autoCorrect="off"
                      spellCheck="false"
                      required
                    />
                    <span
                      className={
                        phoneNumber
                          ? "sign-in-form__label-text sign-in-form__label-text--value"
                          : "sign-in-form__label-text"
                      }
                    >
                      Phone number
                  </span>
                    <span className="sign-in-form__icon">
                      <svg
                        className="feather feather-at-sign sc-dnqmqq jxshSx"
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        aria-hidden="true"
                        data-reactid="156"
                      >
                        <circle cx="12" cy="12" r="4" />
                        <path d="M16 8v5a3 3 0 0 0 6 0v-1a10 10 0 1 0-3.92 7.94" />
                      </svg>
                    </span>
                  </label>
                </div>
                <div className="sign-in-form__inputs" style={{ flex: "1", marginBottom: "15px" }}>
                  <label className="sign-in-form__label">
                    <input
                      className="sign-in-form__input sign-in-form__input--email"
                      onChange={handleChange}
                      type="text"
                      value={country}
                      name="country"
                      autoCorrect="off"
                      spellCheck="false"
                      required
                    />
                    <span
                      className={
                        country
                          ? "sign-in-form__label-text sign-in-form__label-text--value"
                          : "sign-in-form__label-text"
                      }
                    >
                      Country
                  </span>
                    <span className="sign-in-form__icon">
                      <svg
                        className="feather feather-at-sign sc-dnqmqq jxshSx"
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        aria-hidden="true"
                        data-reactid="156"
                      >
                        <circle cx="12" cy="12" r="4" />
                        <path d="M16 8v5a3 3 0 0 0 6 0v-1a10 10 0 1 0-3.92 7.94" />
                      </svg>
                    </span>
                  </label>
                </div>
              </div>
              <div className="sign-in-form__inputs-row" style={{ display: "flex" }}>
                <div className="sign-in-form__inputs" style={{ flex: "1", marginRight: "5%", marginBottom: "15px" }}>
                  <label className="sign-in-form__label">
                    <input
                      className="sign-in-form__input sign-in-form__input--email"
                      onChange={handleChange}
                      type="text"
                      value={city}
                      name="city"
                      autoCorrect="off"
                      spellCheck="false"
                      required
                    />
                    <span
                      className={
                        city
                          ? "sign-in-form__label-text sign-in-form__label-text--value"
                          : "sign-in-form__label-text"
                      }
                    >
                      City
                  </span>
                    <span className="sign-in-form__icon">
                      <svg
                        className="feather feather-at-sign sc-dnqmqq jxshSx"
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        aria-hidden="true"
                        data-reactid="156"
                      >
                        <circle cx="12" cy="12" r="4" />
                        <path d="M16 8v5a3 3 0 0 0 6 0v-1a10 10 0 1 0-3.92 7.94" />
                      </svg>
                    </span>
                  </label>
                </div>
                <div className="sign-in-form__inputs" style={{ flex: "1", marginRight: "5%", marginBottom: "15px" }}>
                  <label className="sign-in-form__label">
                    <input
                      className="sign-in-form__input sign-in-form__input--email"
                      onChange={handleChange}
                      type="text"
                      value={zipCode}
                      name="zipCode"
                      autoCorrect="off"
                      spellCheck="false"
                      required
                    />
                    <span
                      className={
                        zipCode
                          ? "sign-in-form__label-text sign-in-form__label-text--value"
                          : "sign-in-form__label-text"
                      }
                    >
                      Zip code
                  </span>
                    <span className="sign-in-form__icon">
                      <svg
                        className="feather feather-at-sign sc-dnqmqq jxshSx"
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        aria-hidden="true"
                        data-reactid="156"
                      >
                        <circle cx="12" cy="12" r="4" />
                        <path d="M16 8v5a3 3 0 0 0 6 0v-1a10 10 0 1 0-3.92 7.94" />
                      </svg>
                    </span>
                  </label>
                </div>
                <div className="sign-in-form__inputs" style={{ flex: "1", marginBottom: "15px" }}>
                  <label className="sign-in-form__label">
                    <input
                      className="sign-in-form__input sign-in-form__input--email"
                      onChange={handleChange}
                      type="text"
                      value={currentStatus}
                      name="currentStatus"
                      autoCorrect="off"
                      spellCheck="false"
                      required
                    />
                    <span
                      className={
                        currentStatus
                          ? "sign-in-form__label-text sign-in-form__label-text--value"
                          : "sign-in-form__label-text"
                      }
                    >
                      Current status
                  </span>
                    <span className="sign-in-form__icon">
                      <svg
                        className="feather feather-at-sign sc-dnqmqq jxshSx"
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        aria-hidden="true"
                        data-reactid="156"
                      >
                        <circle cx="12" cy="12" r="4" />
                        <path d="M16 8v5a3 3 0 0 0 6 0v-1a10 10 0 1 0-3.92 7.94" />
                      </svg>
                    </span>
                  </label>
                </div>
              </div>
              <div className="sign-in-form__inputs-row" style={{ display: "flex", paddingBottom: "50px" }}>
                <div className="sign-in-form__inputs" style={{ flex: "1", marginRight: "5%", marginBottom: "15px" }}>
                  <label className="sign-in-form__label">
                    <input
                      className="sign-in-form__input sign-in-form__input--email"
                      onChange={handleChange}
                      type="password"
                      value={password}
                      name="password"
                      autoCorrect="off"
                      spellCheck="false"
                      required
                    />
                    <span
                      className={
                        password
                          ? "sign-in-form__label-text sign-in-form__label-text--value"
                          : "sign-in-form__label-text"
                      }
                    >
                      Password
                  </span>
                    <span className="sign-in-form__icon">
                      <svg
                        className="feather feather-at-sign sc-dnqmqq jxshSx"
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        aria-hidden="true"
                        data-reactid="156"
                      >
                        <circle cx="12" cy="12" r="4" />
                        <path d="M16 8v5a3 3 0 0 0 6 0v-1a10 10 0 1 0-3.92 7.94" />
                      </svg>
                    </span>
                  </label>
                </div>
                <div className="sign-in-form__inputs" style={{ flex: "1", marginRight: "5%", marginBottom: "15px" }}>
                  <label className="sign-in-form__label">
                    <input
                      className="sign-in-form__input sign-in-form__input--email"
                      onChange={handleChange}
                      type="password"
                      value={confirmPassword}
                      name="confirmPassword"
                      autoCorrect="off"
                      spellCheck="false"
                      required
                    />
                    <span
                      className={
                        confirmPassword
                          ? "sign-in-form__label-text sign-in-form__label-text--value"
                          : "sign-in-form__label-text"
                      }
                    >Confirm password</span>
                    <span className="sign-in-form__icon">
                      <svg
                        className="feather feather-at-sign sc-dnqmqq jxshSx"
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        aria-hidden="true"
                        data-reactid="156"
                      >
                        <circle cx="12" cy="12" r="4" />
                        <path d="M16 8v5a3 3 0 0 0 6 0v-1a10 10 0 1 0-3.92 7.94" />
                      </svg>
                    </span>
                  </label>
                </div>
                <div style={{ flex: "1" }}></div>
              </div>
              <div className="authentication-page__footer-rights" style={{ display: "flex", justifyContent: "flex-end" }}>
                <button type="submit" style={{ backgroundColor: "#1c85e8", borderRadius: "3px", color: "#ffffff", fontFamily: "Inter UI", fontSize: "14px", fontWeight: "500", padding: "15px 20px", cursor: "pointer" }}>Continue</button>
              </div>
            </form>
          </div>
        </div>
        <div className="authentication-page__image" />
      </div>
    );
  }
}

export default RegistrationDetails;
