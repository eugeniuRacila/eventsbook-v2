import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import axios from "axios";

import RegistrationDetails from "./RegistrationDetails";

export class RegistrationDetailsContainer extends Component {
  state = {
    email: window.location.search.substr(1).split('&')[0].split('=')[1],
    password: "123456",
    confirmPassword: "123456",
    firstName: "Eugeniu",
    lastName: "Răcilă",
    birthDate: 1,
    currentStatus: "Student",
    country: "Moldova",
    city: "Chișinău",
    genre: "Male",
    zipCode: "MD-2075",
    phoneNumber: "37369001122"
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleSubmit = async event => {
    alert('clicked');
    event.preventDefault();

    const {
      email,
      password,
      firstName,
      lastName,
      birthDate,
      currentStatus,
      country,
      city,
      genre,
      zipCode,
      phoneNumber
    } = this.state;

    try {
      await axios.post(
        'http://localhost:5000/api/auth/sign-up-final',
        {
          method: 'local',
          local: {
            email,
            password,
            firstName,
            lastName,
            birthDate,
            currentStatus,
            country,
            city,
            genre,
            zipCode,
            phoneNumber
          }
        }
      );
    } catch (error) {
      console.warn("ERROR :::", error);
    }
  }

  render() {
    const {
      password,
      confirmPassword,
      firstName,
      lastName,
      birthDate,
      currentStatus,
      country,
      city,
      genre,
      zipCode,
      phoneNumber
    } = this.state;

    return (
      <div>
        <RegistrationDetails
          handleChange={this.handleChange}
          handleSubmit={this.handleSubmit}
          password={password}
          confirmPassword={confirmPassword}
          firstName={firstName}
          lastName={lastName}
          birthDate={birthDate}
          currentStatus={currentStatus}
          country={country}
          city={city}
          genre={genre}
          zipCode={zipCode}
          phoneNumber={phoneNumber}
        />
      </div>
    );
  }
}

export default withRouter(RegistrationDetailsContainer);
