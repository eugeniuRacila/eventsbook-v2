import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import axios from "axios";

import Register from "./Register";

export class RegisterContainer extends Component {
  state = {
    email: "",
    emailIsValid: false
  };

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value,
      emailIsValid: event.target.checkValidity()
    });
  };

  handleSubmit = async event => {
    alert('clicked');
    event.preventDefault();

    const { email } = this.state;

    const response = await axios.post(
      'http://localhost:5000/api/auth/sign-up',
      { "email": email }
    );
  }

  render() {
    return (
      <div>
        <Register
          handleChange={this.handleChange}
          handleSubmit={this.handleSubmit}
          emailValue={this.state.email}
          emailIsValid={this.state.emailIsValid}
        />
      </div>
    );
  }
}

export default withRouter(RegisterContainer);
