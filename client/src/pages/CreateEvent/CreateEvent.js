import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import "./CreateEvent.css"

class CreateEvent extends Component {
  render() {
    const {
      onSubmitHandler,
      addParagraph,
      removeParagraph,
      addPicture,
      paragraphsNum,
      changePreview,
      onChangeHandler,
      title,
      category,
      thumbnail,
      date,
      time,
      timeTill,
      country,
      city,
      address,
      price,
      currency,
      maxAmount
    } = this.props;

    return (
      <div className="create-event">
        <form onSubmit={onSubmitHandler} enctype="multipart/form-data">
          <div className="create-event__head">
            <div className="create-event__head_left">
              <p className="create-event__label">Choose event category</p>
              <div>
                <select value={category} name="category" id="" className="create-event__select" onChange={onChangeHandler}>
                  <option value="festival">Festival</option>
                  <option value="dance">Dance</option>
                  <option value="party">Party</option>
                  <option value="concert">Concert</option>
                </select>
              </div>
            </div>
            <div className="create-event__head_right">
              <p className="create-event__label_text">Here's a short guide for you to create the best event you can</p>
              <Link className="create-event__link" to="/blog/event-guide">Manage event creation
            <div>
                  <svg class="feather feather-arrow-right sc-dnqmqq jxshSx" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" aria-hidden="true" data-reactid="131"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>
                </div>
              </Link>
            </div>
          </div>
          <div className="create-event__body">
            <p className="create-event__label">Event title</p>
            <textarea value={title} onChange={onChangeHandler} className="create-event__title" name="title" rows="2" placeholder="Title of the event will be here so you can see it"></textarea>
            <p className="create-event__label">Event thumbnail</p>
            <input id="create-event__file-input" type="file" name="thumbnail" onChange={changePreview} style={{ display: "none" }} />
            <div className="create-event__thumbnail" onClick={addPicture}>
              <img id="create-event__thumbnail-src--id" className="create-event__thumbnail-src" src="" alt="" />
            </div>
            <div className="create-event__description-section" style={{ paddingBottom: "64px" }}>
              <p className="create-event__label">Event description</p>
              <textarea className="create-event__description" name="description" rows="5" placeholder="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque non purus condimentum orci mollis porta. Quisque egestas, justo quis auctor pharetra, lectus urna congue lectus, quis interdum tortor enim in nulla. Sed in ligula libero. Cras fringilla, orci nec maximus blandit, ipsum sem rhoncus orci, eget pulvinar ligula ipsum in quam. Sed egestas, ex id vulputate ornare, dolor elit volutpat nisl, ut sollicitudin lectus enim ac velit. Curabitur vitae posuere sem, et semper lacus. Nullam dignissim risus vitae orci consectetur dignissim."></textarea>
              <div className="create-event__description_actions">
                <button style={{ marginLeft: "0", marginRight: "20px" }} className="create-event__link" onClick={addParagraph}>Add paragraph
              <div>
                    <svg class="feather feather-plus-circle sc-dnqmqq jxshSx" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" aria-hidden="true" data-reactid="976"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                  </div>
                </button>
                {
                  paragraphsNum > 1 &&
                  <button style={{ marginLeft: "0" }} className="create-event__link" onClick={removeParagraph}>Remove paragraph
                  <div>
                      <svg class="feather feather-minus-circle sc-dnqmqq jxshSx" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" aria-hidden="true"><circle cx="12" cy="12" r="10"></circle><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                    </div>
                  </button>
                }
              </div>
            </div>
            <div className="create-event__date-and-time">
              <div className="create-event__date-and-time_section">
                <p className="create-event__label">Date</p>
                <input value={date} onChange={onChangeHandler} className="create-event__input-large" type="text" name="date" placeholder="11.06.2019" />
              </div>
              <div className="create-event__date-and-time_section">
                <p className="create-event__label">Time (FROM)</p>
                <input value={time} onChange={onChangeHandler} className="create-event__input-large" type="text" name="time" placeholder="17:30" />
              </div>
              <div className="create-event__date-and-time_section">
                <p className="create-event__label">Time (TILL)</p>
                <input value={timeTill} onChange={onChangeHandler} className="create-event__input-large" type="text" name="timeTill" placeholder="20:30" />
              </div>
            </div>
            <div className="create-event__date-and-time">
              <div className="create-event__date-and-time_section">
                <p className="create-event__label">Country</p>
                <input value={country} onChange={onChangeHandler} className="create-event__input-large" type="text" name="country" placeholder="Moldova" />
              </div>
              <div className="create-event__date-and-time_section">
                <p className="create-event__label">City</p>
                <input value={city} onChange={onChangeHandler} className="create-event__input-large" type="text" name="city" placeholder="Chișinău" />
              </div>
              <div className="create-event__date-and-time_section">
                <p className="create-event__label">Address</p>
                <input value={address} onChange={onChangeHandler} className="create-event__input-large" type="text" name="address" placeholder="Liviu Deleanu 7/2" />
              </div>
            </div>
            <div className="create-event__date-and-time">
              <div className="create-event__date-and-time_section">
                <p className="create-event__label">Price</p>
                <input value={price} onChange={onChangeHandler} className="create-event__input-large" type="text" name="price" placeholder="0 (Free)" />
              </div>
              <div className="create-event__date-and-time_section">
                <p className="create-event__label">Currency</p>
                <div>
                  <select value={currency} onChange={onChangeHandler} name="currency" className="create-event__select">
                    <option value="mdl">MDL</option>
                    <option value="usd">USD</option>
                    <option value="eur">EUR</option>
                  </select>
                </div>
              </div>
            </div>
            <div className="create-event__date-and-time">
              <div className="create-event__date-and-time_section">
                <p className="create-event__label">Max amount of people</p>
                <input value={maxAmount} onChange={onChangeHandler} className="create-event__input-large" type="text" name="maxAmount" placeholder="20" />
              </div>
            </div>
            <button className="create-event__submit-btn" type="submit">Create a new event</button>
          </div>
        </form>
      </div>
    )
  }
}

export default CreateEvent;