import React, { Component } from 'react';
import axios from 'axios';

import CreateEvent from "./CreateEvent";

class CreateEventContainer extends Component {
  state = {
    paragraphsNum: 1,
    title: "",
    description: [],
    category: "",
    thumbnail: undefined,
    date: "",
    time: "",
    timeTill: "",
    country: "",
    city: "",
    address: "",
    price: "",
    currency: "",
    maxAmount: ""
  }

  onChangeHandler = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  onChangeSelectHandler = (event) => {
    this.setState({
      [event.target.name]: event.options[event.selectedIndex].value
    });
  }

  addParagraph = (event) => {
    event.preventDefault();

    const descriptionsArray = document.getElementsByClassName('create-event__description');

    descriptionsArray[descriptionsArray.length - 1].insertAdjacentHTML(
      'afterend',
      `<textarea class="create-event__description" name="description" rows="5" placeholder="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque non purus condimentum orci mollis porta. Quisque egestas, justo quis auctor pharetra, lectus urna congue lectus, quis interdum tortor enim in nulla. Sed in ligula libero. Cras fringilla, orci nec maximus blandit, ipsum sem rhoncus orci, eget pulvinar ligula ipsum in quam. Sed egestas, ex id vulputate ornare, dolor elit volutpat nisl, ut sollicitudin lectus enim ac velit. Curabitur vitae posuere sem, et semper lacus. Nullam dignissim risus vitae orci consectetur dignissim."></textarea>`
    );

    return this.setState({
      paragraphsNum: this.state.paragraphsNum + 1
    })
  }

  removeParagraph = (event) => {
    event.preventDefault();

    const descriptionsArray = document.getElementsByClassName('create-event__description');

    descriptionsArray[descriptionsArray.length - 1].remove();

    return this.setState({
      paragraphsNum: this.state.paragraphsNum - 1
    })
  }

  addPicture = () => {
    const inputFile = document.getElementById('create-event__file-input');
    inputFile.click();
  }

  changePreview = () => {
    console.log('change preview triggered');
    var preview = document.getElementById('create-event__thumbnail-src--id');
    var file = document.querySelector('input[type=file]').files[0];
    var reader = new FileReader();

    reader.onloadend = () => {
      preview.src = reader.result;
      this.setState({
        thumbnail: file
      });
    }

    if (file) {
      reader.readAsDataURL(file);
    } else {
      preview.src = "";
    }
  }

  onSubmitHandler = (event) => {
    event.preventDefault();

    // Getting all the descriptions
    const descriptionsArray = document.getElementsByClassName('create-event__description');
    const arrOfDescriptionsStrings = [];
    for (let i = 0; i < descriptionsArray.length; i++) {
      arrOfDescriptionsStrings.push(descriptionsArray[i].value);
    }

    const formData = new FormData();

    const {
      title,
      category,
      thumbnail,
      date,
      time,
      timeTill,
      country,
      city,
      address,
      price,
      currency,
      maxAmount
    } = this.state;

    formData.append('thumbnail', thumbnail);
    formData.append('title', title);
    formData.append('category', category);
    formData.append('description', JSON.stringify(arrOfDescriptionsStrings));
    formData.append('date', date);
    formData.append('time', time);
    formData.append('timeTill', timeTill);
    formData.append('country', country);
    formData.append('city', city);
    formData.append('address', address);
    formData.append('price', price);
    formData.append('currency', currency);
    formData.append('maxAmount', maxAmount);
    formData.append('created_by', localStorage.getItem('eventsbook_user_id'));

    axios.post('http://localhost:5000/api/events/create-event', formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Authorization': `Bearer ${localStorage.getItem('eventsbook_access_token')}`
      }
    })
  }

  render() {
    const {
      paragraphsNum,
      title,
      category,
      thumbnail,
      date,
      time,
      timeTill,
      country,
      city,
      address,
      price,
      currency,
      maxAmount
    } = this.state;

    return (
      <CreateEvent
        addParagraph={this.addParagraph}
        removeParagraph={this.removeParagraph}
        addPicture={this.addPicture}
        changePreview={this.changePreview}
        onChangeHandler={this.onChangeHandler}
        onChangeSelectHandler={this.onChangeSelectHandler}
        onSubmitHandler={this.onSubmitHandler}
        paragraphsNum={paragraphsNum}
        title={title}
        category={category}
        thumbnail={thumbnail}
        date={date}
        time={time}
        timeTill={timeTill}
        country={country}
        city={city}
        address={address}
        price={price}
        currency={currency}
        maxAmount={maxAmount}
      />
    )
  }
}

export default CreateEventContainer;
