import React, { Component } from "react";

import Main from './Main';

import "./Main.css";

class MainContainer extends Component {
  render() {
    return (
      <Main />
    );
  }
}

export default MainContainer;
