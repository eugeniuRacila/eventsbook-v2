import React, { Component } from "react";

import "./Main.css";
import soldSvg from "../../assets/sold.svg";
import cashSvg from "../../assets/cash.svg";
import calendarSvg from "../../assets/calendar.svg";

class Main extends Component {
  render() {
    return (
      <div className="main">
        <section className="main-hero">
          <div className="blue-block">
            <p className="blue-block__label">Break old habits</p>
            <h1 className="blue-block__title">Welcome to a better way to find the events you're interested in.</h1>
            <div className="blue-block__search-string">
              <div className="blue-block__search-string-fields">
                I want to{' '}
                <select name="" id="">
                  <option value="">party and dance</option>
                  <option value="">chill</option>
                  <option value="">dance</option>
                  <option value="">hike</option>
                </select>
                {' '}located on{' '}
                <select name="" id="">
                  <option value="">Moldova, Chisinau</option>
                  <option value="">Moldova, Chisinau</option>
                  <option value="">Moldova, Balti</option>
                  <option value="">Moldova, Orhei</option>
                </select>
              </div>
              <button className="blue-block__button">Get started</button>
            </div>
          </div>
        </section>
        <section className="eventsbook-message">
          <h2 className="eventsbook-message__text">We believe in a better event selling and buying experience</h2>
        </section>
        <section className="why-us">
          <div className="why-us__container">
            <div className="why-us__block">
              <img src={soldSvg} alt="sold tickets" className="why-us__icon" />
              <h2 className="why-us__title">Convenient ticket sale</h2>
              <p className="why-us__description">Sell your home without listing, showings, and months of stress</p>
            </div>
            <div className="why-us__block">
              <img src={cashSvg} alt="sold tickets" className="why-us__icon" />
              <h2 className="why-us__title">Save money and time</h2>
              <p className="why-us__description">Sell your home without listing, showings, and months of stress</p>
            </div>
            <div className="why-us__block">
              <img src={calendarSvg} alt="sold tickets" className="why-us__icon" />
              <h2 className="why-us__title">Time management</h2>
              <p className="why-us__description">Sell your home without listing, showings, and months of stress</p>
            </div>
          </div>
        </section>
        <section className="sign-to-event">
          <p className="sign-to-event__label">Sign to event in a few simple steps</p>
          <div className="sign-to-event__steps-block">
            <div className="sign-to-event__step">
              <p className="sign-to-event__step-number">1</p>
              <p className="sign-to-event__step-description">​​Enter your home details and get an offer in just a few clicks</p>
            </div>
            <div className="sign-to-event__step">
              <p className="sign-to-event__step-number">2</p>
              <p className="sign-to-event__step-description">​​Enter your home details and get an offer in just a few clicks</p>
            </div>
            <div className="sign-to-event__step">
              <p className="sign-to-event__step-number">3</p>
              <p className="sign-to-event__step-description">​​Enter your home details and get an offer in just a few clicks</p>
            </div>
          </div>
          <div className="sign-to-event__video"></div>
        </section>
      </div>
    )
  }
}

export default Main;
