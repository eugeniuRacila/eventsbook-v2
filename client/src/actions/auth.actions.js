import axios from "axios";



/* --- Fetching initial auth details (when user enters the webpage) --- */

export const FETCH_AUTH_BEGIN = "FETCH_AUTH_BEGIN";
export const FETCH_AUTH_SUCCESS = "FETCH_AUTH_SUCCESS";
export const FETCH_AUTH_ERROR = "FETCH_AUTH_ERROR";

export const fetchAuthBegin = () => ({
  type: FETCH_AUTH_BEGIN
});

export const fetchAuthSuccess = auth => ({
  type: FETCH_AUTH_SUCCESS,
  payload: { auth }
});

export const fetchAuthError = error => ({
  type: FETCH_AUTH_ERROR,
  payload: { error }
});

export const fetchAuth = () => {
  console.log("init fetchAuth");
  return async dispatch => {
    try {
      const accessToken = await localStorage.getItem("eventsbook_access_token");
      const userId = await localStorage.getItem("eventsbook_user_id");

      if (!accessToken && !userId) return;

      dispatch(fetchAuthBegin());

      const response = await axios({
        method: "GET",
        url: `http://localhost:5000/api/users/${userId}`
      });

      switch (response.status) {
        case 200:
          dispatch(fetchAuthSuccess(JSON.parse(response.request.response)));
          return await JSON.parse(response.request.response);

        default:
          return dispatch(fetchAuthError(response.status));
      }
    } catch (error) {
      return dispatch(fetchAuthError(error));
    }
  };
};



/* --- Fetching the user details on log in --- */

export const LOGIN_AUTH_BEGIN = "LOGIN_AUTH_BEGIN";
export const LOGIN_AUTH_SUCCESS = "LOGIN_AUTH_SUCCESS";
export const LOGIN_AUTH_ERROR = "LOGIN_AUTH_ERROR";
export const CLEAR_LOGIN_AUTH_ERROR = "CLEAR_LOGIN_AUTH_ERROR";

export const fetchLoginAuthBegin = () => ({
  type: LOGIN_AUTH_BEGIN
});

export const fetchLoginAuthSuccess = auth => ({
  type: LOGIN_AUTH_SUCCESS,
  payload: { auth }
});

export const fetchLoginAuthError = error => ({
  type: LOGIN_AUTH_ERROR,
  payload: { error }
});

export const clearFetchLoginAuthError = () => ({
  type: CLEAR_LOGIN_AUTH_ERROR
});

export const fetchLoginAuth = (body) => {
  console.log("init fetch login auth");
  return async dispatch => {
    try {
      dispatch(fetchLoginAuthBegin());

      const response = await axios.post(
        'http://localhost:5000/api/auth/sign-in',
        {
          "email": body.email,
          "password": body.password
        }
      );

      switch (response.status) {
        case 200:
          localStorage.setItem('eventsbook_access_token', response.data.token);
          localStorage.setItem('eventsbook_user_id', response.data.id);
          await dispatch(fetchLoginAuthSuccess(response.data));
          await dispatch(fetchAuth());
          break;

        default:
          return dispatch(fetchLoginAuthError(response.status));
      }
    } catch (error) {
      console.log('errror ::', error);
      return dispatch(fetchLoginAuthError(error));
    }
  };
};