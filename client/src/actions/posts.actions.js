import axios from "axios";

export const FETCH_POSTS_BEGIN = "FETCH_POSTS_BEGIN";
export const FETCH_POSTS_SUCCESS = "FETCH_POSTS_SUCCESS";
export const FETCH_POSTS_ERROR = "FETCH_POSTS_ERROR";

export const fetchPostsBegin = () => ({
  type: FETCH_POSTS_BEGIN
});

export const fetchPostsSuccess = posts => ({
  type: FETCH_POSTS_SUCCESS,
  payload: { posts }
});

export const fetchPostsError = error => ({
  type: FETCH_POSTS_ERROR,
  payload: { error }
});

export const fetchPosts = () => {
  return async dispatch => {
    console.log("Posts fetch");
    dispatch(fetchPostsBegin());

    try {
      const response = await axios({
        method: "GET",
        url: "https://jsonplaceholder.typicode.com/posts"
      });

      switch (response.status) {
        case 200:
          dispatch(fetchPostsSuccess(JSON.parse(response.request.response)));
          return await JSON.parse(response.request.response);

        default:
          return dispatch(fetchPostsError(response.status));
      }
    } catch (error) {
      return dispatch(fetchPostsError(error));
    }
  };
};
